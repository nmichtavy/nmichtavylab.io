import logo from "./logo.jpeg";
import "./App.css";
import { TypeAnimation } from "react-type-animation";

function App() {
	return (
		<>
			<nav className="bg-black">
				<div className="flex w-full flex-wrap items-center justify-between mx-auto">
					<h1 className="pl-5 pt-2 font-poppins text-lg bg-black text-sky-400 first-letter:text-white font-medium">
						Nolan Michtavy
					</h1>
					<a
						className="block font-poppins text-lg bg-black text-sky-400 font-medium pr-5 pt-2 underline"
						href="mailto:nmichtavy@gmail.com"
					>
						Send me an email
					</a>
				</div>
			</nav>
			<div className="relative bg-black border-b-8 border-sky-400">
				<header className="flex justify-center pb-6">
					<img src={logo} className="w-96 h-96" alt="logo" />
				</header>
			</div>
			<div className="flex w-1/2 justify-center ml-8">
				<TypeAnimation
					sequence={[
						"Hi there!",
						() => {
							console.log("Sequence completed");
						},
					]}
					wrapper="span"
					cursor={true}
					className="flex w-fit p-8 font-poppins font-medium text-8xl
				tracking-tighter"
				/>
			</div>
		</>
	);
}

export default App;
