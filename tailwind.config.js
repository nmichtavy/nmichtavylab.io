/** @type {import('tailwindcss').Config} */

module.exports = {
	content: ["./src/**/*.{html,js,jsx}"],
	theme: {
		extend: {
            fontFamily: {
                roboto: ['Roboto', 'sans-serif'],
                poppins: ['Poppins', 'sans-serif'],
            },
        },
	},
	plugins: [],
};
